/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Request;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.NGOCoordinatorOrganization;
import Business.Organization.Organization;
import Business.Organization.RehabilitationOrganization;
import Business.User.User;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.RehabRequest;
import Common.Validation;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author arosk
 */
public class WorkRequestRehab extends javax.swing.JPanel {

    /**
     * Creates new form WorkRequestAmbulance
     */
    
    private JPanel userProcessContainer;
    
    private UserAccount account;
    private NGOCoordinatorOrganization organization;
    private Enterprise enterprise;
    private EcoSystem system;
    private Network network;

    public WorkRequestRehab(JPanel userProcessContainer, UserAccount account, Enterprise enterprise, EcoSystem system, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        //    this.organization = organization;
        this.enterprise = enterprise;
        this.system = system;
        this.network = new Network();
      
        loadCmbNetwork();
        loadUsers();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNetwork = new javax.swing.JLabel();
        cmbNetwork = new javax.swing.JComboBox<>();
        lblArea = new javax.swing.JLabel();
        cmbArea = new javax.swing.JComboBox<>();
        lblTitle = new javax.swing.JLabel();
        lblSelectUser = new javax.swing.JLabel();
        cmbUser = new javax.swing.JComboBox<>();
        btnBack = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        lblEnterDateTime = new javax.swing.JLabel();
        txtMM = new javax.swing.JTextField();
        lblDetailsOfIllness = new javax.swing.JLabel();
        txtDetailsIllness = new javax.swing.JTextField();
        txtDD = new javax.swing.JTextField();
        txtYYYY = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        lblNetwork.setText("Select Network:");

        cmbNetwork.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNetworkItemStateChanged(evt);
            }
        });
        cmbNetwork.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbNetworkActionPerformed(evt);
            }
        });

        lblArea.setText("Select Area:");

        lblTitle.setFont(new java.awt.Font("Imprint MT Shadow", 1, 24)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 153, 153));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("Create Request For Rehabilitation Facility");

        lblSelectUser.setText("Select User:");

        cmbUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbUserActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(0, 153, 153));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/back.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(51, 204, 0));
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/add.png"))); // NOI18N
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        lblEnterDateTime.setText("Enter Date(Perferred):");

        lblDetailsOfIllness.setText("Details of Illness:");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("(MM)");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("(DD)");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("(YYYY)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(151, 151, 151)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(lblNetwork, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblArea, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblSelectUser, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblEnterDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblDetailsOfIllness, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cmbNetwork, 0, 258, Short.MAX_VALUE)
                                .addComponent(cmbArea, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbUser, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                                        .addComponent(txtMM, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtDD)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtYYYY)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)))
                                .addComponent(txtDetailsIllness)))))
                .addContainerGap(276, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNetwork, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbNetwork, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblArea, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbArea, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSelectUser, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbUser, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblEnterDateTime, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(txtMM)
                    .addComponent(txtDD)
                    .addComponent(txtYYYY))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDetailsIllness, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDetailsOfIllness, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(248, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    public void loadCmbNetwork() {
        cmbNetwork.removeAllItems();
        for (Network network : system.getNetworkList()) {
            cmbNetwork.addItem(network.getName());
        }
        cmbNetwork.setSelectedIndex(0);
    }

    public void loadCmbArea(Network network) {
        cmbArea.removeAllItems();
        for (String area : network.getAreas()) {
            cmbArea.addItem(area);
        }
        cmbArea.setSelectedIndex(0);
    }

    public void loadUsers() {
        cmbUser.removeAllItems();
        if (system.getUserList().isEmpty()) {
            JOptionPane.showMessageDialog(null, "No user found");
        } else {
            for (User user : system.getUserList()) {
                cmbUser.addItem(user.getUniqueUserName());
            }
            cmbUser.setSelectedIndex(0);
        }
    }


    private void cmbNetworkItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNetworkItemStateChanged
        String networkName = String.valueOf(cmbNetwork.getSelectedItem());
        this.network = this.system.getNetworkList().stream().filter(x -> x.getName().equalsIgnoreCase(networkName)).findFirst().orElse(null);
        if (this.network == null) {
            JOptionPane.showMessageDialog(null, "No network found");
            return;
        } else {
            loadCmbArea(this.network);
        }
    }//GEN-LAST:event_cmbNetworkItemStateChanged

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        Validation.resetErrorHighlight(txtDD, lblEnterDateTime);
        Validation.resetErrorHighlight(txtMM, lblEnterDateTime);
        Validation.resetErrorHighlight(txtYYYY, lblEnterDateTime);

        String netwrk = cmbNetwork.getItemAt(0);
        String area = cmbArea.getSelectedItem().toString();
        String uniqueUserName = cmbUser.getSelectedItem().toString();

        String illness = txtDetailsIllness.getText();

        User user = this.system.getUserList().stream().filter(x -> x.getUniqueUserName().equalsIgnoreCase(uniqueUserName)).findFirst().orElse(null);

        Date date = Validation.validateDate(txtMM.getText() + "/" + txtDD.getText() + "/" + txtYYYY.getText());
        if (date == null) {
            Validation.HighlightError(txtDD, lblEnterDateTime);
            Validation.HighlightError(txtMM, lblEnterDateTime);
            Validation.HighlightError(txtYYYY, lblEnterDateTime);
            JOptionPane.showMessageDialog(null, "Please enter date in correct format");
            return;
        }
        if (!Validation.compareDate(date)) {
            Validation.HighlightError(txtDD, lblEnterDateTime);
            Validation.HighlightError(txtMM, lblEnterDateTime);
            Validation.HighlightError(txtYYYY, lblEnterDateTime);
            JOptionPane.showMessageDialog(null, "Appointment date can not be less than today's date.");
            return;
        }

        RehabRequest rehabRequest = new RehabRequest();
        rehabRequest.setNetwork(netwrk);
        rehabRequest.setArea(area);
        rehabRequest.setUser(user);
        rehabRequest.setDateTime(date);
        rehabRequest.setSender(account);
        rehabRequest.setStatus("New");

        Organization org = null;
        for (Organization organization1 : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization1 instanceof RehabilitationOrganization) {
                org = organization1;
                break;
            }
        }

        if (org != null) {
            org.getWorkQueue().getWorkRequestList().add(rehabRequest);
            account.getWorkQueue().getWorkRequestList().add(rehabRequest);
            user.getUserRequest().add(rehabRequest);
        }
        JOptionPane.showMessageDialog(null, "Request submitted successfully !!");
    }//GEN-LAST:event_btnAddActionPerformed

    private void cmbNetworkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbNetworkActionPerformed

    }//GEN-LAST:event_cmbNetworkActionPerformed

    private void cmbUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbUserActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JComboBox<String> cmbArea;
    private javax.swing.JComboBox<String> cmbNetwork;
    private javax.swing.JComboBox<String> cmbUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblArea;
    private javax.swing.JLabel lblDetailsOfIllness;
    private javax.swing.JLabel lblEnterDateTime;
    private javax.swing.JLabel lblNetwork;
    private javax.swing.JLabel lblSelectUser;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTextField txtDD;
    private javax.swing.JTextField txtDetailsIllness;
    private javax.swing.JTextField txtMM;
    private javax.swing.JTextField txtYYYY;
    // End of variables declaration//GEN-END:variables
}
