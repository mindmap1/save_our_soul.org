/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.EnterpriseAdminWorkArea;

import Business.Enterprise.Enterprise;
import Business.Organization.AmbulanceOrganization;
import Business.Organization.BloodBankOrganization;
import Business.Organization.ChemistOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.Organization.NGOCoordinatorOrganization;
import Business.Organization.Organization;
import Business.Organization.RehabilitationOrganization;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author phapa
 */
public class OrganizationListHome extends javax.swing.JPanel {

    /**
     * Creates new form OrganizationListHome
     */
    JPanel userProcessContainer;
    Enterprise enterprise;

    public OrganizationListHome(JPanel userProcessContainer, Enterprise enterprise) {

        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        populate();
        List<String> organizationList = new ArrayList<>();
        cmbOrg.setVisible(false);
        btnAdd.setVisible(false);
        cmbOrg.removeAllItems();
        if (enterprise.getEnterpriseType().getValue().equalsIgnoreCase(Enterprise.EnterpriseType.Hospital.getValue())) {
            organizationList.add("Doctor");
            organizationList.add("Lab");
            organizationList.add("Blood Bank");
            organizationList.add("Ambulance");
            for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                organizationList.remove(org.getName());
            }
            if (organizationList.size() > 0) {
                cmbOrg.setVisible(true);
                btnAdd.setVisible(true);
                for (String item : organizationList) {
                    cmbOrg.addItem(item);
                }
                lblNote.setVisible(false);
            } else {
                lblNote.setText("All Organization are present.");
            }
        }
        if (enterprise.getEnterpriseType().getValue().equalsIgnoreCase(Enterprise.EnterpriseType.NGO.getValue())) {
            organizationList.add("Staff");
            organizationList.add("Rehab");
            for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                organizationList.remove(org.getName());
            }
            if (organizationList.size() > 0) {
                cmbOrg.setVisible(true);
                btnAdd.setVisible(true);
                for (String item : organizationList) {
                    cmbOrg.addItem(item);
                }
                lblNote.setVisible(false);
            } else {
                lblNote.setText("All Organization present.");
            }
        }
        if (enterprise.getEnterpriseType().getValue().equalsIgnoreCase(Enterprise.EnterpriseType.Pharmacy.getValue())) {
            organizationList.add("Pharma Staff");
            for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                organizationList.remove(org.getName());
            }
            if (organizationList.size() > 0) {
                cmbOrg.setVisible(true);
                btnAdd.setVisible(true);
                for (String item : organizationList) {
                    cmbOrg.addItem(item);
                }
                lblNote.setVisible(false);
            } else {
                lblNote.setText("All Organization present.");
            }
        }
    }

    public void populate() {
        DefaultTableModel dtm = (DefaultTableModel) tblOrgList.getModel();
        dtm.setRowCount(0);
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
            Object[] row = new Object[2];
            row[0] = org;
            row[1] = enterprise;
            dtm.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrgList = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        lblNote = new javax.swing.JLabel();
        cmbOrg = new javax.swing.JComboBox<>();
        btnBack = new javax.swing.JButton();

        lblTitle.setFont(new java.awt.Font("Imprint MT Shadow", 1, 24)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 153, 153));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("Organization List");

        tblOrgList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Organization Name", "Enterprise Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblOrgList);
        if (tblOrgList.getColumnModel().getColumnCount() > 0) {
            tblOrgList.getColumnModel().getColumn(0).setResizable(false);
            tblOrgList.getColumnModel().getColumn(1).setResizable(false);
        }

        btnDelete.setBackground(new java.awt.Color(255, 0, 51));
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/delete.png"))); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(51, 204, 0));
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/add.png"))); // NOI18N
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        lblNote.setFont(new java.awt.Font("Tahoma", 2, 13)); // NOI18N

        btnBack.setBackground(new java.awt.Color(0, 153, 153));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/back.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cmbOrg, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblTitle, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
                    .addComponent(lblNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(197, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbOrg, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(lblNote, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(258, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String OrgName = String.valueOf(cmbOrg.getSelectedItem());
        if (OrgName.equalsIgnoreCase("Doctor")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new DoctorOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Lab")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new LabOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Blood Bank")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new BloodBankOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Ambulance")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new AmbulanceOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Staff")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new NGOCoordinatorOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Rehab")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new RehabilitationOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        if (OrgName.equalsIgnoreCase("Pharma Staff")) {
            enterprise.getOrganizationDirectory().getOrganizationList().add(new ChemistOrganization(OrgName, enterprise.getCity(), enterprise.getArea()));
        }
        JOptionPane.showMessageDialog(null, "Operation Successful.");
        cmbOrg.removeItem(OrgName);
        if (cmbOrg.getSelectedItem() == null) {
            btnAdd.setVisible(false);
            cmbOrg.setVisible(false);
            lblNote.setVisible(true);
        }
        populate();
        
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedRow = tblOrgList.getSelectedRow();
        if (selectedRow >= 0) {
            int option = JOptionPane.showConfirmDialog(null, "Do you want to proceed?", "Delete organization", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (option == 0) {
                Organization og = (Organization) tblOrgList.getValueAt(selectedRow, 0);
                og.getEmployeeDirectory().getEmployeeList().clear();
                og.getUserAccountDirectory().getUserAccountList().clear();
                this.enterprise.getOrganizationDirectory().getOrganizationList().remove(og);
                JOptionPane.showMessageDialog(null, "Operation Successful.");
                cmbOrg.addItem(og.getName());
                cmbOrg.setVisible(true);
                lblNote.setVisible(false);
                btnAdd.setVisible(true);
                populate();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select organization to be deleted.");
            return;
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)this.userProcessContainer.getLayout();
        layout.previous(this.userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JComboBox<String> cmbOrg;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNote;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTable tblOrgList;
    // End of variables declaration//GEN-END:variables
}
