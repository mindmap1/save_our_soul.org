/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ChemistWorkArea;

import Business.Enterprise.Enterprise;
import Business.Organization.ChemistOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author phapa
 */
public class ChemistWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ChemistWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount account;
    private ChemistOrganization organization;
    private Enterprise enterprise;
    public ChemistWorkAreaJPanel(JPanel userProcessContainer,UserAccount account, ChemistOrganization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.organization = organization;
        this.enterprise = enterprise;
        lblTitle.setText("Welcome "+account.getEmployee().getName());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblTitle = new javax.swing.JLabel();
        btnMyCases = new javax.swing.JButton();
        btnAllRequest = new javax.swing.JButton();
        btnNewRequest = new javax.swing.JButton();

        lblTitle.setFont(new java.awt.Font("Imprint MT Shadow", 1, 24)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 153, 153));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("Welcome,");

        btnMyCases.setText("My Cases");
        btnMyCases.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMyCasesActionPerformed(evt);
            }
        });

        btnAllRequest.setText("All Request");
        btnAllRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllRequestActionPerformed(evt);
            }
        });

        btnNewRequest.setText("New Request");
        btnNewRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewRequestActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(281, 281, 281)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 611, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnNewRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(btnAllRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(btnMyCases, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(461, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addGap(64, 64, 64)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnMyCases, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAllRequest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnNewRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(373, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnMyCasesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMyCasesActionPerformed
        int count = 0;
        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request.getReceiver() == null) {
                continue;
            }
            if (request.getReceiver().getEmployee().getName().equals(account.getEmployee().getName())) {
                count++;
            }

        }
        if (count == 0) {
            JOptionPane.showMessageDialog(null, "Currently no request's are assigned on your workqueue");
            return;

        } else {
            ChemistMyCases chemistMyCases = new ChemistMyCases(userProcessContainer, account, organization, enterprise);
            userProcessContainer.add("chemistMyCases", chemistMyCases);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_btnMyCasesActionPerformed

    private void btnAllRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllRequestActionPerformed
        int count = 0;
        if (organization.getWorkQueue().getWorkRequestList().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Currently no  workqueue history present for your organization");
            return;
        } else {
            ChemistAllRequest chemistAllRequest = new ChemistAllRequest(userProcessContainer, account, organization);
            userProcessContainer.add("chemistAllRequest", chemistAllRequest);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_btnAllRequestActionPerformed

    private void btnNewRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewRequestActionPerformed
        int count = 0;
        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request.getStatus().equalsIgnoreCase("New")) {
                count++;
            }
        }
        if (count == 0) {
            JOptionPane.showMessageDialog(null, "Currently no new request's are assigned on your workqueue");
            return;

        } else {
            ChemistNewRequest chemistNewRequest = new ChemistNewRequest(userProcessContainer, account, organization);
            userProcessContainer.add("chemistNewRequest", chemistNewRequest);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_btnNewRequestActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAllRequest;
    private javax.swing.JButton btnMyCases;
    private javax.swing.JButton btnNewRequest;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}
