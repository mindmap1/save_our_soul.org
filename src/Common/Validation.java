/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;


/**
 *
 * @author phapa
 */
public class Validation {

    static Logger log = Logger.getLogger(Validation.class.getName());

    //password format validation
    public static Boolean passwordFormatValidation(String password) {
        log.error("Invalid password creation took place.");
        return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
        
    }

    public static void resetErrorHighlight(JTextComponent txt, JLabel lbl) {
        txt.setBorder(new JTextField().getBorder());
        lbl.setForeground(Color.BLACK);
    }

    public static void HighlightError(JTextComponent txt, JLabel lbl) {
        log.error("Invalid text input.");
        txt.setBorder(new LineBorder(Color.RED));
        lbl.setForeground(Color.RED);
    }

    public static boolean isValidTextField(String value) {
        log.error("Invalid text input");
        return value == null ? false : !value.trim().isEmpty();
        
    }

    public static boolean validUsername(EcoSystem ecosystem, String username) {
        if (ecosystem.getUserAccountDirectory().getUserAccountList().stream().anyMatch(x -> x.getUsername().equalsIgnoreCase(username))) {
            return false;
        }
        for (Network network : ecosystem.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (enterprise.getUserAccountDirectory().getUserAccountList().stream().anyMatch(x -> x.getUsername().equalsIgnoreCase(username))) {
                    log.error("Invalid user creation takes place.");
                    return false;
                }
                for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (org.getUserAccountDirectory().getUserAccountList().stream().anyMatch(x -> x.getUsername().equalsIgnoreCase(username))) {
                        log.error("Invalid user creation takes place.");
                        return false;
                        
                    }
                }
            }
        }

        return true;
    }

    public static Date validateDate(String date) {
        try {
            return new SimpleDateFormat("MM/dd/yyyy").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean compareDate(Date date) {
        return date.compareTo(new Date()) >= 0;
    }

    public static boolean validateNumber(String number) {
        try {
            return number.matches("\\d+");
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean validatePhoneNumber(String number) {
        try {
            return number.matches("\\d{10}");
        } catch (Exception e) {
            return false;
        }
    }
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX
            = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean checkPresent(JComboBox cmb, String value) {

        int length = cmb.getItemCount();
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (String.valueOf(cmb.getItemAt(i)).equalsIgnoreCase(value)) {
                    log.error("Values present already.");
                    return false;
                    
                }
            }
        }
        return true;

    }
}
