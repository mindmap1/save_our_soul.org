/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

/**
 *
 * @author phapa
 */
public class SendWhatsAppMessage {
    static Logger log = Logger.getLogger(SendWhatsAppMessage.class.getName());
    public static final String ACCOUNT_SID = "AC6403da41700fd8e50e07a9bee2f33aff";
    public static final String AUTH_TOKEN = "73002d4a7f6591bbb4efe7f63a1fccba";

    public SendWhatsAppMessage() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }
    
    public void send(String to, String messageText){
        try {
            Message message = Message.creator(
            new PhoneNumber(to),
            new PhoneNumber("whatsapp:+14155238886"),
            messageText)
            .create();
        }
        catch(Exception ex){
            log.error("Message not delivered.");
            System.err.println("Numbers are not in correctly formatted");
            return;
        }
    }
    
}
