/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

/**
 *
 * @author phapa
 */
public class SendEmailService {

    StringBuilder messageText;
    static Logger log = Logger.getLogger(SendEmailService.class.getName());

    public SendEmailService() {
        this.messageText = new StringBuilder();
    }

    public static String getHeader() {
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "\n"
                + "<header>\n"
                + "<h1 style=\"width:100%;color:white;\"><span style=\"width:100%;background-color:darkcyan\">Save Our Soul Org</span></h1>\n"
                + "</header><body><p>Dear,</p></br>";
    }

    public static String getFooter() {
        return "</body></br><footer>\n"
                + "<p style=\"margin:0;\">Sincerely,</p>\n"
                + "<p style=\"margin:0;\"><i>NGO Coordinator</i></p>\n"
                + "<p style=\"margin:0;\"><b>Save our soul org</b></p>\n"
                + "<p style=\"margin:0\"><em style=\"opacity:0.6;\">saveoursoulngo.org@gmail.com<em></p>\n"
                + "\n"
                + "</footer>\n"
                + "\n"
                + "</html>";
    }

    public String send(String ToEmail, String Subject, StringBuilder Text) {

        String Msg;
        StringBuilder finalText = new StringBuilder();
        finalText.append(getHeader());
        finalText.append(Text);
        finalText.append(getFooter());

        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("saveoursoulngo.org@gmail.com", "Ingram@123");
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("saveoursoulngo.org@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(ToEmail));//u will send to
            message.setSubject(Subject);
            message.setContent(finalText.toString(), "text/html; charset=utf-8");
            Transport.send(message);
            Msg = "true";
            return Msg;

        } catch (Exception e) {
            log.error("Email service failed.");
            return e.toString();
        }

    }

}
