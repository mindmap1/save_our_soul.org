/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Enterprise.EnterpriseType;
import Business.Network.Network;
import Business.Organization.AmbulanceOrganization;
import Business.Organization.BloodBankOrganization;
import Business.Organization.ChemistOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.Organization.NGOCoordinatorOrganization;
import Business.Organization.Organization;
import Business.Organization.RehabilitationOrganization;
import Business.Role.AmbulanceRole;
import Business.Role.BloodBankRole;
import Business.Role.ChemistRole;
import Business.Role.DoctorRole;
import Business.Role.EnterpriseAdminRole;
import Business.Role.LabAssistantRole;
import Business.Role.NGOCoordinatorRole;
import Business.Role.RehabRole;
import Business.Role.SystemAdminRole;
import Business.User.User;
import Business.UserAccount.UserAccount;

/**
 *
 * @author manja
 */
public class ConfigureSystem {

    public static EcoSystem configure() {

        EcoSystem system = EcoSystem.getInstance();

        //Create a network
        Network networkBoston = new Network();
        networkBoston.setName("Boston-SOS");
        networkBoston.setCity("Boston");
        networkBoston.getAreas().add("Allston");
        networkBoston.getAreas().add("Back Bay");
        networkBoston.getAreas().add("Bay Village");
        networkBoston.getAreas().add("Beacon Hill");
        networkBoston.getAreas().add("Brighton");
        networkBoston.getAreas().add("Charlestown");
        networkBoston.getAreas().add("Chinatown");
        networkBoston.getAreas().add("Dorchester");
        networkBoston.getAreas().add("East Boston");
        networkBoston.getAreas().add("Fenway Kenmore");
        networkBoston.getAreas().add("Hyde Park");
        networkBoston.getAreas().add("Jamica Plain");
        networkBoston.getAreas().add("Mattapan");
        networkBoston.getAreas().add("Mission Hill");
        networkBoston.getAreas().add("North End");
        networkBoston.getAreas().add("Roslindale");
        networkBoston.getAreas().add("Roxbury");
        networkBoston.getAreas().add("South Boston");
        networkBoston.getAreas().add("South End");
        networkBoston.getAreas().add("West End");
        networkBoston.getAreas().add("West Roxbury");

        //create an enterprise hospital list
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Atrius Health", networkBoston.getCity(), networkBoston.getAreas().get(9), Enterprise.EnterpriseType.Hospital);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Dana Farber Hospital", networkBoston.getCity(), networkBoston.getAreas().get(9), Enterprise.EnterpriseType.Hospital);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("NEBH", networkBoston.getCity(), networkBoston.getAreas().get(13), Enterprise.EnterpriseType.Hospital);

        //Create an enterprise NGO list
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Fenway Community Development Corporation", networkBoston.getCity(), networkBoston.getAreas().get(9), Enterprise.EnterpriseType.NGO);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Partners In Health", networkBoston.getCity(), networkBoston.getAreas().get(1), Enterprise.EnterpriseType.NGO);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Mission Hill Health Movement", networkBoston.getCity(), networkBoston.getAreas().get(13), Enterprise.EnterpriseType.NGO);

        //Create an enterprise pharmacy list
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("CVS Pharmacy", networkBoston.getCity(), networkBoston.getAreas().get(9), Enterprise.EnterpriseType.Pharmacy);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Brighton Pharmacy", networkBoston.getCity(), networkBoston.getAreas().get(4), Enterprise.EnterpriseType.Pharmacy);
        networkBoston.getEnterpriseDirectory().createAndAddEnterprise("Reliance Pharmacy", networkBoston.getCity(), networkBoston.getAreas().get(13), Enterprise.EnterpriseType.Pharmacy);

        //create admin login for each of above 3 Enterprises
        //adding hospital 1, 2 and 3 admin
        Enterprise enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(9))
                && x.getName().equalsIgnoreCase("Atrius Health") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        Employee emp = enterprise.getEmployeeDirectory().createEmployee("Atrius Admin");
        enterprise.getUserAccountDirectory().createUserAccount("entadmin", "entadmin", emp, new EnterpriseAdminRole());

        //add organization in current enterprise
        enterprise.getOrganizationDirectory().getOrganizationList().add(new DoctorOrganization("Doctor", enterprise.getCity(), enterprise.getArea()));
        enterprise.getOrganizationDirectory().getOrganizationList().add(new LabOrganization("Lab", enterprise.getCity(), enterprise.getArea()));
        enterprise.getOrganizationDirectory().getOrganizationList().add(new BloodBankOrganization("Blood Bank", enterprise.getCity(), enterprise.getArea()));
        enterprise.getOrganizationDirectory().getOrganizationList().add(new AmbulanceOrganization("Ambulance", enterprise.getCity(), enterprise.getArea()));

        //adding users to above organizations
        //1. Doctors
        Organization og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Doctor")).findFirst().orElse(null);
        Employee emp2 = og.getEmployeeDirectory().createEmployee("Dr. Stanley");
        og.getUserAccountDirectory().createUserAccount("druser1", "drpassword1", emp2, new DoctorRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Dr. Johnson");
        og.getUserAccountDirectory().createUserAccount("druser2", "drpassword2", emp2, new DoctorRole());

        //2. Lab
        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Lab")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Drx. Rose");
        og.getUserAccountDirectory().createUserAccount("labuser1", "labpassword1", emp2, new LabAssistantRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Drx. Tina");
        og.getUserAccountDirectory().createUserAccount("labuser2", "labpassword2", emp2, new LabAssistantRole());

        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Blood Bank")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Dr. Mike");
        og.getUserAccountDirectory().createUserAccount("bloodbankuser1", "bloodbankpassword1", emp2, new BloodBankRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Dr. Nick");
        og.getUserAccountDirectory().createUserAccount("bloodbankuser2", "bloodbankpassword2", emp2, new BloodBankRole());

        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Ambulance")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Driver Robert");
        og.getUserAccountDirectory().createUserAccount("driveruser1", "driverpassword1", emp2, new AmbulanceRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Driver John");
        og.getUserAccountDirectory().createUserAccount("driveruser2", "driverpassword2", emp2, new AmbulanceRole());
        //

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(9))
                && x.getName().equalsIgnoreCase("Dana Farber Hospital") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("Dana Farber Hospital Admin");
        enterprise.getUserAccountDirectory().createUserAccount("danafarberadmin", "danafarberpassword", emp, new EnterpriseAdminRole());

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(13))
                && x.getName().equalsIgnoreCase("NEBH") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("NEBH Admin");
        enterprise.getUserAccountDirectory().createUserAccount("nebhadmin", "nebhpassword", emp, new EnterpriseAdminRole());

        //adding NGO 1 2, and 3 admin
        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(9))
                && x.getName().equalsIgnoreCase("Fenway Community Development Corporation") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("FCDC Admin");
        enterprise.getUserAccountDirectory().createUserAccount("fcdcadmin", "fcdcpassword", emp, new EnterpriseAdminRole());

        //adding organization in current NGO
        enterprise.getOrganizationDirectory().getOrganizationList().add(new NGOCoordinatorOrganization("Staff", enterprise.getCity(), enterprise.getArea()));
        enterprise.getOrganizationDirectory().getOrganizationList().add(new RehabilitationOrganization("Rehab", enterprise.getCity(), enterprise.getArea()));

        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Staff")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Mr. Ron");
        og.getUserAccountDirectory().createUserAccount("staffuser1", "staffpassword1", emp2, new NGOCoordinatorRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Mrs. Sheela");
        og.getUserAccountDirectory().createUserAccount("staffuser2", "staffpassword2", emp2, new NGOCoordinatorRole());

        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Rehab")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Mr. James");
        og.getUserAccountDirectory().createUserAccount("rehabuser1", "rehabpassword1", emp2, new RehabRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Mrs. Handan");
        og.getUserAccountDirectory().createUserAccount("rehabuser2", "rehabpassword2", emp2, new RehabRole());

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(1))
                && x.getName().equalsIgnoreCase("Partners In Health") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("Partners Admin");
        enterprise.getUserAccountDirectory().createUserAccount("partnersadmin", "partnerspassword", emp, new EnterpriseAdminRole());

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(13))
                && x.getName().equalsIgnoreCase("Mission Hill Health Movement") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("Mission Hill Health Movement Admin");
        enterprise.getUserAccountDirectory().createUserAccount("mhhmadmin", "mhhmpassword", emp, new EnterpriseAdminRole());

        //adding pharma 1,2 and 3 admin
        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(9))
                && x.getName().equalsIgnoreCase("CVS Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("CVS Admin");
        enterprise.getUserAccountDirectory().createUserAccount("cvsadmin", "cvspassword", emp, new EnterpriseAdminRole());

        //Adding organization to the Pharma
        enterprise.getOrganizationDirectory().getOrganizationList().add(new ChemistOrganization("Pharma Staff", enterprise.getCity(), enterprise.getArea()));

        og = enterprise.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Pharma staff")).findFirst().orElse(null);
        emp2 = og.getEmployeeDirectory().createEmployee("Mr. Randy");
        og.getUserAccountDirectory().createUserAccount("chemistuser1", "chemistpassword1", emp2, new ChemistRole());

        emp2 = og.getEmployeeDirectory().createEmployee("Mrs. Wheeler");
        og.getUserAccountDirectory().createUserAccount("chemistuser2", "chemistpassword2", emp2, new ChemistRole());

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(4))
                && x.getName().equalsIgnoreCase("Brighton Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("Brighton Admin");
        enterprise.getUserAccountDirectory().createUserAccount("brightonadmin", "brightonpassword", emp, new EnterpriseAdminRole());

        enterprise = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(13))
                && x.getName().equalsIgnoreCase("Reliance Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp = enterprise.getEmployeeDirectory().createEmployee("Reliance Admin");
        enterprise.getUserAccountDirectory().createUserAccount("relianceadmin", "reliancepassword", emp, new EnterpriseAdminRole());

//        //creating work requests for blood donation - NGO 
//        BloodDonationWorkRequest bwrq = new BloodDonationWorkRequest();
//        bwrq.setDonor_name("Mr. Jack Ryan");
//        bwrq.setDonor_blood_group("B+");
//        bwrq.setRequested_area("Mission Hill");
//
//        bwrq.setMessage("I want to donate blood");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            bwrq.setRequestDate(sdf.parse("2019-11-26"));
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//        bwrq.setResolveDate(null);
//        //bwrq.setSender(null);
//        bwrq.setStatus("pending");
//        Enterprise enterprise5 = networkBoston.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Boston")
//                && x.getArea().equalsIgnoreCase(networkBoston.getAreas().get(9))
//                && x.getName().equalsIgnoreCase("Fenway Community Development Corporation") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
//        og = enterprise5.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Staff")).findFirst().orElse(null);
//
//        og.getWorkQueue().getWorkRequestList().add(bwrq);
        //initialize some organizations
        //Adding organizations to only one enterprizes from each section 
        //i.e. Atrius Health(Hospital) and Fenway Community Development Corporation(NGO) and  CVS Pharmacy

        //have some employees 
        //create user account
        system.getNetworkList().add(networkBoston);
        Employee employee = system.getEmployeeDirectory().createEmployee("System Admin");
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());

        User user = system.createAndAddUser();
        user.setFirstName("Vignesh");
        user.setLastName("Vaidyanathan");
        user.setAge(25);
        user.setGender("Male");
        user.setEmailId("vaidhyanathan.v@husky.neu.edu");
        user.setBloodGroup("A+");
        user.setPhoneNumber("8573338102");
        user.setUniqueUserName(user.getFirstName()+"_"+user.getLastName()+"_"+user.getUserId());
        
        // -------------------------------------------NETWORK NYC -----------------------------------------------------------------------
        
        //create new network New York
        Network networkNewYork = new Network();
        networkNewYork.setName("NewYork-SOS");
        networkNewYork.setCity("NewYork");
        networkNewYork.getAreas().add("Concourse");
        networkNewYork.getAreas().add("Longwood");
        networkNewYork.getAreas().add("Highbridge");
        networkNewYork.getAreas().add("Claremont");
        networkNewYork.getAreas().add("Brooklyn");
        networkNewYork.getAreas().add("Chelsea");
        networkNewYork.getAreas().add("Manhattan");
        networkNewYork.getAreas().add("Queens");
        networkNewYork.getAreas().add("Harlem");
        networkNewYork.getAreas().add("Bronx");
        networkNewYork.getAreas().add("Staten Island");
        networkNewYork.getAreas().add("Tribeca");
        networkNewYork.getAreas().add("Central Park");
        networkNewYork.getAreas().add("Midtown");
        networkNewYork.getAreas().add("Greenwich Village");
        networkNewYork.getAreas().add("Flatiron");
        networkNewYork.getAreas().add("Fordham");
        networkNewYork.getAreas().add("Belmont");
        networkNewYork.getAreas().add("Norwood");
        
        //adding hospitals to nyc
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Bellevue Hospital Centre", networkNewYork.getCity(), networkNewYork.getAreas().get(5), EnterpriseType.Hospital);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Lower Manhattan Hospital", networkNewYork.getCity(), networkNewYork.getAreas().get(7), EnterpriseType.Hospital);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Lenox Hill Hospital", networkNewYork.getCity(), networkNewYork.getAreas().get(11), EnterpriseType.Hospital);
        
        //adding ngo to nyc
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Physicians for Human Rights", networkNewYork.getCity(), networkNewYork.getAreas().get(3), EnterpriseType.NGO);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Idealist.org", networkNewYork.getCity(), networkNewYork.getAreas().get(8), EnterpriseType.NGO);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("NGO Working Group On Women, Peace and Security", networkNewYork.getCity(), networkNewYork.getAreas().get(12), EnterpriseType.NGO);
       
        //adding pharmacy in nyc
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("New York City Pharmacy", networkNewYork.getCity(), networkNewYork.getAreas().get(12), EnterpriseType.Pharmacy);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("Walgreens", networkNewYork.getCity(), networkNewYork.getAreas().get(2), EnterpriseType.Pharmacy);
        networkNewYork.getEnterpriseDirectory().createAndAddEnterprise("New London Pharmacy", networkNewYork.getCity(), networkNewYork.getAreas().get(5), EnterpriseType.Pharmacy);
        
        //create admin login for the above 3 enterprises
        //addin admin1,2,3 to hospital enterprise
        Enterprise enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(5))
                && x.getName().equalsIgnoreCase("Bellevue Hospital Centre") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        Employee emp1 = enterprise1.getEmployeeDirectory().createEmployee("Bellevue Admin");
        enterprise1.getUserAccountDirectory().createUserAccount("bellevueadmin", "bellevuepassword", emp1, new EnterpriseAdminRole());
        
        //add organization in current enterprise
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new DoctorOrganization("Doctor", enterprise.getCity(), enterprise.getArea()));
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new LabOrganization("Lab", enterprise.getCity(), enterprise.getArea()));
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new BloodBankOrganization("Blood Bank", enterprise.getCity(), enterprise.getArea()));
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new AmbulanceOrganization("Ambulance", enterprise.getCity(), enterprise.getArea()));
        
        //adding users to above organizations
        //1. Doctors
        Organization og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Doctor")).findFirst().orElse(null);
        Employee emp3= og1.getEmployeeDirectory().createEmployee("Dr. Mitch");
        og1.getUserAccountDirectory().createUserAccount("druser1bel", "drpassword1bel", emp3, new DoctorRole());

        emp3 = og1.getEmployeeDirectory().createEmployee("Dr. Carell");
        og1.getUserAccountDirectory().createUserAccount("druser2bel", "drpassword2bel", emp3, new DoctorRole());

        //2. Lab
        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Lab")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Drx. Stanley");
        og1.getUserAccountDirectory().createUserAccount("labuser1bel", "labpassword1bel", emp3, new LabAssistantRole());

        emp3= og1.getEmployeeDirectory().createEmployee("Drx. Max");
        og1.getUserAccountDirectory().createUserAccount("labuser2bel", "labpassword2bel", emp3, new LabAssistantRole());

        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Blood Bank")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Dr. Rick");
        og1.getUserAccountDirectory().createUserAccount("bloodbankuser1bel", "bloodbankpassword1bel", emp3, new BloodBankRole());

        emp3 = og.getEmployeeDirectory().createEmployee("Dr. Jane");
        og1.getUserAccountDirectory().createUserAccount("bloodbankuser2bel", "bloodbankpassword2bel", emp3, new BloodBankRole());

        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Ambulance")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Driver James");
        og1.getUserAccountDirectory().createUserAccount("driveruser1bel", "driverpassword1bel", emp3, new AmbulanceRole());

        emp3 = og1.getEmployeeDirectory().createEmployee("Driver Harry");
        og1.getUserAccountDirectory().createUserAccount("driveruser2bel", "driverpassword2bel", emp3, new AmbulanceRole());
        
        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(7))
                && x.getName().equalsIgnoreCase("Lower Manhattan Hospital") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("Lower Manhattan Admin");
        enterprise1.getUserAccountDirectory().createUserAccount("lowermanhattanadmin", "lowermanhattanpassword", emp1, new EnterpriseAdminRole());

        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(11))
                && x.getName().equalsIgnoreCase("Lenox Hill Hospital") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("Lenox Hill Admin");
        enterprise1.getUserAccountDirectory().createUserAccount("lenoxadmin", "lenoxpassword", emp1, new EnterpriseAdminRole());
        
        //adding NGO 1 2, and 3 admin
        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(3))
                && x.getName().equalsIgnoreCase("Physicians For Human Rights") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("PhysiciansforhumanAdmin");
        enterprise1.getUserAccountDirectory().createUserAccount("phyadmin", "phypassword", emp1, new EnterpriseAdminRole());

        //adding organization in current NGO
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new NGOCoordinatorOrganization("Staff", enterprise.getCity(), enterprise.getArea()));
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new RehabilitationOrganization("Rehab", enterprise.getCity(), enterprise.getArea()));

        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Staff")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Mr. Josh");
        og1.getUserAccountDirectory().createUserAccount("staffuser1phy", "staffpassword1phy", emp3, new NGOCoordinatorRole());

        emp3 = og1.getEmployeeDirectory().createEmployee("Mrs. Reena");
        og1.getUserAccountDirectory().createUserAccount("staffuser2phy", "staffpassword2phy", emp3, new NGOCoordinatorRole());

        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Rehab")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Mr. Ken");
        og1.getUserAccountDirectory().createUserAccount("rehabuser1phy", "rehabpassword1phy", emp3, new RehabRole());

        emp3 = og1.getEmployeeDirectory().createEmployee("Mrs. Chai");
        og1.getUserAccountDirectory().createUserAccount("rehabuser2phy", "rehabpassword2phy", emp3, new RehabRole());

        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(8))
                && x.getName().equalsIgnoreCase("Idealist.org") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("IdealistAdmin");
        enterprise1.getUserAccountDirectory().createUserAccount("Idealistadmin", "Idealistpassword", emp1, new EnterpriseAdminRole());

        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(12))
                && x.getName().equalsIgnoreCase("NGO Working Group On Women, Peace and Security") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("NGOWorkingWomenAdmin");
        enterprise1.getUserAccountDirectory().createUserAccount("wwadmin", "wwpassword", emp1, new EnterpriseAdminRole());

        //adding pharma 1,2 and 3 admin
        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(12))
                && x.getName().equalsIgnoreCase("New York City Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("NYCAdmin");
        enterprise1.getUserAccountDirectory().createUserAccount("nycadmin", "nycpassword", emp1, new EnterpriseAdminRole());

        //Adding organization to the Pharma
        enterprise1.getOrganizationDirectory().getOrganizationList().add(new ChemistOrganization("Pharma Staff", enterprise.getCity(), enterprise.getArea()));

        og1 = enterprise1.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Pharma staff")).findFirst().orElse(null);
        emp3 = og1.getEmployeeDirectory().createEmployee("Mr. Ron");
        og1.getUserAccountDirectory().createUserAccount("chemistuser1nyc", "chemistpassword1nyc", emp3, new ChemistRole());

        emp3 = og1.getEmployeeDirectory().createEmployee("Mrs. Wonder");
        og1.getUserAccountDirectory().createUserAccount("chemistuser2nyc", "chemistpassword2nyc", emp3, new ChemistRole());

        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(2))
                && x.getName().equalsIgnoreCase("Walgreens") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("WalAdmin");
        enterprise1.getUserAccountDirectory().createUserAccount("walgreensadmin", "walgreenspassword", emp1, new EnterpriseAdminRole());

        enterprise1 = networkNewYork.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("NewYork")
                && x.getArea().equalsIgnoreCase(networkNewYork.getAreas().get(5))
                && x.getName().equalsIgnoreCase("New London Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp1 = enterprise1.getEmployeeDirectory().createEmployee("New London Admin");
        enterprise1.getUserAccountDirectory().createUserAccount("londonadmin", "lomdonpassword", emp1, new EnterpriseAdminRole());
        
                // -------------------------------------------NETWORK CHICAGO-------------------------------------------------------------
        
        //create new network Chicago
        Network networkChicago = new Network();
        networkChicago.setName("Chicago-SOS");
        networkChicago.setCity("Chicago");
        networkChicago.getAreas().add("Chatham");
        networkChicago.getAreas().add("Avalon Park");
        networkChicago.getAreas().add("Loyola");
        networkChicago.getAreas().add("Nortown");
        networkChicago.getAreas().add("Peterson Park");
        networkChicago.getAreas().add("Rosehill");
        networkChicago.getAreas().add("Edison Park");
        networkChicago.getAreas().add("Northcenter");
        networkChicago.getAreas().add("North Halsted");
        networkChicago.getAreas().add("Avondale");
        networkChicago.getAreas().add("Bucktown");
        networkChicago.getAreas().add("Portage Park");
        networkChicago.getAreas().add("Irving Woods");
        networkChicago.getAreas().add("Montclare");
        networkChicago.getAreas().add("The Gold Coast");
        networkChicago.getAreas().add("Dearborn Park");
        networkChicago.getAreas().add("Austin");
        networkChicago.getAreas().add("Greektown");
        networkChicago.getAreas().add("West Elsdon");
        
        
        
        //adding hospitals to chicago
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Northwestern Memorial Hospital", networkChicago.getCity(), networkChicago.getAreas().get(4), EnterpriseType.Hospital);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Methodist Hospital of Chicago", networkChicago.getCity(), networkChicago.getAreas().get(8), EnterpriseType.Hospital);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Norwegian American Hospital", networkChicago.getCity(), networkChicago.getAreas().get(9), EnterpriseType.Hospital);
        
        //adding ngo to chicago
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("CARE", networkChicago.getCity(), networkChicago.getAreas().get(10), EnterpriseType.NGO);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Concern Worldwide US Inc", networkChicago.getCity(), networkChicago.getAreas().get(12), EnterpriseType.NGO);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Chicago Global Health Alliance", networkChicago.getCity(), networkChicago.getAreas().get(13), EnterpriseType.NGO);
       
        //adding pharmacy in chicago
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Well Future Pharmacy", networkChicago.getCity(), networkChicago.getAreas().get(10), EnterpriseType.Pharmacy);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Deitch Pharmacy", networkChicago.getCity(), networkChicago.getAreas().get(2), EnterpriseType.Pharmacy);
        networkChicago.getEnterpriseDirectory().createAndAddEnterprise("Xpress Pharmacy Inc", networkChicago.getCity(), networkChicago.getAreas().get(4), EnterpriseType.Pharmacy);
        
        //create admin login for the above 3 enterprises
        //addin admin1,2,3 to hospital enterprise
        Enterprise enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(4))
                && x.getName().equalsIgnoreCase("Northwestern Memorial Hospital") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        Employee emp4 = enterprise2.getEmployeeDirectory().createEmployee("Northwestern Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("nwadmin", "nwpassword", emp4, new EnterpriseAdminRole());
        
        //add organization in current enterprise
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new DoctorOrganization("Doctor", enterprise.getCity(), enterprise.getArea()));
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new LabOrganization("Lab", enterprise.getCity(), enterprise.getArea()));
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new BloodBankOrganization("Blood Bank", enterprise.getCity(), enterprise.getArea()));
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new AmbulanceOrganization("Ambulance", enterprise.getCity(), enterprise.getArea()));
        
        //adding users to above organizations
        //1. Doctors
        Organization og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Doctor")).findFirst().orElse(null);
        Employee emp5= og2.getEmployeeDirectory().createEmployee("Dr. Michael");
        og2.getUserAccountDirectory().createUserAccount("druser1nw", "drpassword1belnw", emp5, new DoctorRole());

        emp4 = og2.getEmployeeDirectory().createEmployee("Dr. Cavili");
        og2.getUserAccountDirectory().createUserAccount("druser2nw", "drpassword2nw", emp4, new DoctorRole());

        //2. Lab
        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Lab")).findFirst().orElse(null);
        emp4 = og2.getEmployeeDirectory().createEmployee("Drx. Roy");
        og2.getUserAccountDirectory().createUserAccount("labuser1nw", "labpassword1nw", emp4, new LabAssistantRole());

        emp4= og2.getEmployeeDirectory().createEmployee("Drx. Oly");
        og2.getUserAccountDirectory().createUserAccount("labuser2nw", "labpassword2nw", emp4, new LabAssistantRole());

        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Blood Bank")).findFirst().orElse(null);
        emp4 = og1.getEmployeeDirectory().createEmployee("Dr. kevin");
        og2.getUserAccountDirectory().createUserAccount("bloodbankuser1nw", "bloodbankpassword1nw", emp4, new BloodBankRole());

        emp4 = og2.getEmployeeDirectory().createEmployee("Dr. Joy");
        og2.getUserAccountDirectory().createUserAccount("bloodbankuser2nw", "bloodbankpassword2nw", emp4, new BloodBankRole());

        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Ambulance")).findFirst().orElse(null);
        emp4 = og2.getEmployeeDirectory().createEmployee("Driver David");
        og2.getUserAccountDirectory().createUserAccount("driveruser1nw", "driverpassword1nw", emp4, new AmbulanceRole());

        emp4 = og1.getEmployeeDirectory().createEmployee("Driver Hillyard");
        og2.getUserAccountDirectory().createUserAccount("driveruser2nw", "driverpassword2nw", emp4, new AmbulanceRole());
        
        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(8))
                && x.getName().equalsIgnoreCase("Methodist Hospital of Chicago") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp2 = enterprise1.getEmployeeDirectory().createEmployee("Methodist Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("mhadmin", "mhpassword", emp2, new EnterpriseAdminRole());

        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(9))
                && x.getName().equalsIgnoreCase("Norwegian American Hospital") && x.getEnterpriseType().equals(EnterpriseType.Hospital)).findFirst().orElse(null);
        emp2 = enterprise1.getEmployeeDirectory().createEmployee("Norwegian Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("norwegianadmin", "norwegianpassword", emp2, new EnterpriseAdminRole());
        
        //adding NGO 1 2, and 3 admin
        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(10))
                && x.getName().equalsIgnoreCase("CARE") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp2 = enterprise2.getEmployeeDirectory().createEmployee("careAdmin");
        enterprise2.getUserAccountDirectory().createUserAccount("careadmin", "carepassword", emp2, new EnterpriseAdminRole());

        //adding organization in current NGO
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new NGOCoordinatorOrganization("Staff", enterprise.getCity(), enterprise.getArea()));
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new RehabilitationOrganization("Rehab", enterprise.getCity(), enterprise.getArea()));

        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Staff")).findFirst().orElse(null);
        emp4 = og2.getEmployeeDirectory().createEmployee("Mr. Jon");
        og2.getUserAccountDirectory().createUserAccount("staffuser1care", "staffpassword1care", emp4, new NGOCoordinatorRole());

        emp4 = og2.getEmployeeDirectory().createEmployee("Mrs. Rachel");
        og2.getUserAccountDirectory().createUserAccount("staffuser2care", "staffpassword2care", emp4, new NGOCoordinatorRole());

        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Rehab")).findFirst().orElse(null);
        emp4 = og2.getEmployeeDirectory().createEmployee("Mrs. Kelly");
        og2.getUserAccountDirectory().createUserAccount("rehabuser1care", "rehabpassword1care", emp4, new RehabRole());

        emp4 = og2.getEmployeeDirectory().createEmployee("Mrs. Cajun");
        og2.getUserAccountDirectory().createUserAccount("rehabuser2care", "rehabpassword2care", emp4, new RehabRole());

        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(12))
                && x.getName().equalsIgnoreCase("Concern Worldwide US Inc") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp2 = enterprise1.getEmployeeDirectory().createEmployee("concern Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("concernadmin", "concernpassword", emp2, new EnterpriseAdminRole());

        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(13))
                && x.getName().equalsIgnoreCase("Chicago Global Health Alliance") && x.getEnterpriseType().equals(EnterpriseType.NGO)).findFirst().orElse(null);
        emp2 = enterprise2.getEmployeeDirectory().createEmployee("chicagoglobal Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("cgadmin", "cgpassword", emp2, new EnterpriseAdminRole());

        //adding pharma 1,2 and 3 admin
        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(10))
                && x.getName().equalsIgnoreCase("Well Future Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp2 = enterprise2.getEmployeeDirectory().createEmployee("well Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("wfadmin", "wfpassword", emp2, new EnterpriseAdminRole());

        //Adding organization to the Pharma
        enterprise2.getOrganizationDirectory().getOrganizationList().add(new ChemistOrganization("Pharma Staff", enterprise.getCity(), enterprise.getArea()));

        og2 = enterprise2.getOrganizationDirectory().getOrganizationList().stream().filter(x -> x.getName().equalsIgnoreCase("Pharma staff")).findFirst().orElse(null);
        emp4 = og2.getEmployeeDirectory().createEmployee("Mr. Nev");
        og2.getUserAccountDirectory().createUserAccount("chemistuser1well", "chemistpassword1well", emp4, new ChemistRole());

        emp4 = og2.getEmployeeDirectory().createEmployee("Mrs. Wou");
        og2.getUserAccountDirectory().createUserAccount("chemistuser2well", "chemistpassword2well", emp4, new ChemistRole());

        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(2))
                && x.getName().equalsIgnoreCase("Deitch Pharmacy") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp2 = enterprise2.getEmployeeDirectory().createEmployee("DeitchAdmin");
        enterprise2.getUserAccountDirectory().createUserAccount("deitchadmin", "deitchpassword", emp2, new EnterpriseAdminRole());

        enterprise2 = networkChicago.getEnterpriseDirectory().getEnterpriseList().stream().filter(x -> x.getCity().equalsIgnoreCase("Chicago")
                && x.getArea().equalsIgnoreCase(networkChicago.getAreas().get(4))
                && x.getName().equalsIgnoreCase("Xpress Pharmacy Inc") && x.getEnterpriseType().equals(EnterpriseType.Pharmacy)).findFirst().orElse(null);
        emp2 = enterprise2.getEmployeeDirectory().createEmployee("Xpress Admin");
        enterprise2.getUserAccountDirectory().createUserAccount("xpressadmin", "xpresspassword", emp2, new EnterpriseAdminRole());
        
        system.getNetworkList().add(networkNewYork);
        system.getNetworkList().add(networkChicago);
   
        return system;
    }
}
