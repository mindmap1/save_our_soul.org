/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.NGOCoordinatorRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author phapa
 */
public class NGOCoordinatorOrganization extends Organization {

    public NGOCoordinatorOrganization(String name, String city, String area) {
        super(name, city, area);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new NGOCoordinatorRole());
        return roles;
    }
    
}
