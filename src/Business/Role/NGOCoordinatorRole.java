/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.NGOCoordinatorOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.NGOWorkArea.NGOStaffWorkAreaPanel;
import javax.swing.JPanel;

/**
 *
 * @author phapa
 */
public class NGOCoordinatorRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem system) {
        
        return new NGOStaffWorkAreaPanel(userProcessContainer, account, (NGOCoordinatorOrganization) organization, enterprise, system);
    }
    
}
