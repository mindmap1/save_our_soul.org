/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.ChemistOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.ChemistWorkArea.ChemistWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author phapa
 */
public class ChemistRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new ChemistWorkAreaJPanel(userProcessContainer, account, (ChemistOrganization)organization, enterprise);
    }
    
    
}
