///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package Business.WorkQueue;

import Business.User.User;
import Business.UserAccount.UserAccount;
import java.util.Date;

///**
// *
// * @author manja
// */
public class WorkRequest {

    private String message;
    private UserAccount sender;
    private UserAccount receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private User user;
    private String details;
    private String testName;
    private String sensoryScreen;
    private String immunization;
    private String developmentAssesment;
    private String anticipantoryGuidence;

    public WorkRequest() {
        requestDate = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return this.getSender().getEmployee().getName();
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getSensoryScreen() {
        return sensoryScreen;
    }

    public void setSensoryScreen(String sensoryScreen) {
        this.sensoryScreen = sensoryScreen;
    }

    public String getImmunization() {
        return immunization;
    }

    public void setImmunization(String immunization) {
        this.immunization = immunization;
    }

    public String getDevelopmentAssesment() {
        return developmentAssesment;
    }

    public void setDevelopmentAssesment(String developmentAssesment) {
        this.developmentAssesment = developmentAssesment;
    }

    public String getAnticipantoryGuidence() {
        return anticipantoryGuidence;
    }

    public void setAnticipantoryGuidence(String anticipantoryGuidence) {
        this.anticipantoryGuidence = anticipantoryGuidence;
    }

}
