/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author arosk
 */
public class RehabRequest extends WorkRequest {

    private String network;
    private String area;
    private Date dateTime;
    private String detailsOfIllness;

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

  public String getDetailsOfIllness() {
        return detailsOfIllness;
    }

    public void setDetailsOfIllness(String detailsOfIllness) {
        this.detailsOfIllness = detailsOfIllness;
    }

}
