/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author manja
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseList;
   

    public ArrayList<Enterprise> getEnterpriseList() {
        return this.enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public EnterpriseDirectory(){
        this.enterpriseList=new ArrayList<Enterprise>();
    }
    
    //Create enterprise
    public Enterprise createAndAddEnterprise(String name,String city, String area,Enterprise.EnterpriseType type){
        Enterprise enterprise=null;
        if(type==Enterprise.EnterpriseType.Hospital){
            Enterprise newhospital =  (Enterprise)new HospitalEnterprise(name,city, area);
            enterpriseList.add(newhospital);
        }
        if(type==Enterprise.EnterpriseType.NGO){
            Enterprise newNGO =  (Enterprise)new NGOEnterprise(name,city, area);
            enterpriseList.add(newNGO);
        }
        if(type==Enterprise.EnterpriseType.Pharmacy){
            Enterprise newPharma = (Enterprise) new PharmacyEnterprise(name, city, area);
            enterpriseList.add(newPharma);
        }
        return enterprise;
    }
    
    public void deleteEnterprise(Enterprise enterprise){
        enterpriseList.remove(enterprise);
    }
}
