/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author arosk
 */
public class PharmacyEnterprise extends Enterprise{
    public PharmacyEnterprise(String name, String city, String area){
        super(name,city,area,Enterprise.EnterpriseType.Pharmacy);
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
}
